function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
}

function renderDSSV(arr) {
  var contentHTML = "";

  arr.forEach((sv) => {
    var contentTr = `<tr>
    <td>${sv.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${Math.floor(
      (sv.math * 1 + sv.physics * 1 + sv.chemistry * 1) / 3
    )}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.id
    }')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.id
    }')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
function showThongTinRaForm(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physics;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}
function timKiemViTri(id, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].id == id) {
      return i;
    }
  }
  return -1;
}
