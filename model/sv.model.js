var SinhVien = function (id, name, email, password, math, physics, chemistry) {
  this.id = id;
  this.name = name;
  this.email = email;
  this.password = password;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;

  this.tinhDTB = function () {
    return (
      Math.round(
        ((this.math * 1 + this.physics * 1 + this.chemistry * 1) / 3) * 100
      ) / 100
    );
  };
};
