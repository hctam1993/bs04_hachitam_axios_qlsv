const BASE_URL = "https://62db6cb5e56f6d82a7728608.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      console.log("res_delete", res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function themSV() {
  var newSV = layThongTinTuForm();

  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function reset() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}
var index = -1; //vị trí sv
var getIdSua = "";
function suaSinhVien(id) {
  document.getElementById("btn_capnhat").disabled = false;
  document.getElementById("txtMaSV").disabled = true;
  getIdSua = id;
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res: ", res);
      showThongTinRaForm(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function capnhatSV() {
  var newSV = new layThongTinTuForm();
  document.getElementById("btn_capnhat").disabled = true;
  document.getElementById("txtMaSV").disabled = false;

  axios({
    url: `${BASE_URL}/sv/${getIdSua}`,
    method: "PUT",
    data: newSV,
  })
    .then(function (res) {
      console.log("res: ", res);
      getDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function showTenTim() {
  var tenSearch = document.getElementById("txtSearch").value;

  batLoading();
  axios({
    url: `${BASE_URL}/sv?name=${tenSearch}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
